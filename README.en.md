# JavaWeb_Ant_Pro

#### Description
是一款基于SpringBoot2、MybatisPlus、Shiro、Vue、AntDesign等技术栈研发的前后端分离开发框架，采用全新的架构设计，后端服务和前端都是采用全新的设计方案，兼容手机、PAD和PC电脑端，拥有完善的(RBAC)权限架构，权限控制精细化到按钮节点级别颗粒度控制，系统框架集成了代码生成器，一键生成CRUD整个模块代码，可以用户个人项目、公司项目以及客户定制化项目！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
