// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.system.common.BaseController;
import com.javaweb.system.entity.Example2;
import com.javaweb.system.query.Example2Query;
import com.javaweb.system.service.IExample2Service;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 演示案例表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2021-04-30
 */
@RestController
@RequestMapping("/example2")
public class Example2Controller extends BaseController {

    @Autowired
    private IExample2Service example2Service;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @RequiresPermissions("sys:example2:index")
    @GetMapping("/index")
    public JsonResult index(Example2Query query) {
        return example2Service.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示案例表", logType = LogType.INSERT)
    @RequiresPermissions("sys:example2:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Example2 entity) {
        return example2Service.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param example2Id 记录ID
     * @return
     */
    @GetMapping("/info/{example2Id}")
    public JsonResult info(@PathVariable("example2Id") Integer example2Id) {
        return example2Service.info(example2Id);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示案例表", logType = LogType.UPDATE)
    @RequiresPermissions("sys:example2:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Example2 entity) {
        return example2Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param example2Ids 记录ID
     * @return
     */
    @Log(title = "演示案例表", logType = LogType.DELETE)
    @RequiresPermissions("sys:example2:drop")
    @DeleteMapping("/delete/{example2Ids}")
    public JsonResult delete(@PathVariable("example2Ids") Integer[] example2Ids) {
        return example2Service.deleteByIds(example2Ids);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示案例表", logType = LogType.STATUS)
    @RequiresPermissions("sys:example2:status")
    @PutMapping("/setStatus")
    public JsonResult setStatus(@RequestBody Example2 entity) {
        return example2Service.setStatus(entity);
    }
}